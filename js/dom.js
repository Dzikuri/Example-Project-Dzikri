// DOM MANIPULATION
var div = $(".test");
var span = div.find("span");

span.html("<b>tulisannya ganti</b>");
console.log(span.html()); // ambil content

span.attr("bgcolor", "blue"); // set attribute
console.log(span.attr("bgcolor")); // ambil attribute

span.css({"color": "#09f", background: "#f90", "min-height": "50px"})
span.css({"color": "#f00", "display": "block"})

div.addClass("nambah");
div.removeClass("test");

var p = $("<p>Ini Content Tambahan</p>");
// append
// prepend
// insertAfter
// insertBefore
// div.prepend(p);
// p.insertBefore(span);

var span_clone = span.clone();
span_clone.html("hasil clone");

$(document.body).prepend(span_clone);

span_clone.remove();