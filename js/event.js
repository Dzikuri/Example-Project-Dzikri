// MOUSE
// click
$(".test span").on("click", function(e) {
	console.log(e);

	alert("wow");
})

// mouseup
// mousedown
// mouseover
$(".test span").on("mouseover", function(e) {
	console.log(e.clientX, e.clientY);
})

// contextmenu
$(document.body).on("contextmenu", function(e) {
	// Buat Element
	// Append dengan coordinate click
	
	confirm("apakah anda yakin ? ");
	e.preventDefault();
})
	
// KEYBOARD
// keyup
$("[name='email']").on("keyup", function(e) {
	var value = $(this).val();

	if (!value.match(/\w+?@\w+?\.\w+/)) {
		console.log("invalid");
	}
	else console.log("valid");
	// console.log()
})

// keypress
$(document.body).on("keypress", function(e) {
	// console.log(e.keyCode);
	if (e.keyCode == 13) {
		alert("pencet enter ya ?");
	}
})

// keydown - override event default
$(document.body).on("keydown", function(e) {
	if (e.keyCode == 83 && e.ctrlKey) {
		e.preventDefault();
		alert("disable ctrl+s");
	}
})